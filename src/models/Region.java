package models;

public class Region {
     private String regionCode;
     private String regionName;

     public Region(String regionCode, String regionName) {
          this.regionCode = regionCode;
          this.regionName = regionName;
     }

     public String getRegionCode() {
          return regionCode;
     }

     public String getRegionName() {
          return regionName;
     }
}
