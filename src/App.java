import java.util.ArrayList;

import models.Country;
import models.Region;

public class App {
    public static void main(String[] args) throws Exception {
        //khởi tạo danh sách country
        ArrayList<Country> countries = new ArrayList<>();

        //Tạo các region cho viêt nam
        Country vietnam = new Country("VN", "Việt Nam");
        vietnam.addRegion(new Region("SG", "Sài Gòn"));
        vietnam.addRegion(new Region("HN", "Hà Nội"));
        vietnam.addRegion(new Region("DN", "Đà Nẵng"));
        countries.add(vietnam);

         // In ra console các country
         for (Country country : countries) {
            System.out.println("Country Code: " + country.getCountryCode());
            System.out.println("Country Name: " + country.getCountryName());
        }

         // Duyệt danh sách country và in danh sách regions của Việt Nam ra console
         for (Country country : countries) {
            if (country.getCountryName().equals("Việt Nam")) {
                System.out.println("Regions of Việt Nam:");
                for (Region region : country.getRegions()) {
                    System.out.println("Region Code: " + region.getRegionCode());
                    System.out.println("Region Name: " + region.getRegionName());
                }
            }
        }
        
    }
}
